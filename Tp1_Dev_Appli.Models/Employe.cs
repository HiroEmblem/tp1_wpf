﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Tp1_Dev_Appli.Model
{
    public class Employe : INotifyPropertyChanged
    {
        public int Id { get; set; }

        public string FirstName 
        { get; set; }
        

        public string LastName { get; set; }

        public int CardId { get; set; }

        public Employe(int id, string fn, string ln, int cardid)
        {
            this.Id = id;
            this.FirstName = fn;
            this.LastName = ln;
            this.CardId = cardid;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)

        {

            if (PropertyChanged != null)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(propName));

            }

        }
    }
}