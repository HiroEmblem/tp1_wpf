﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Tp1_Dev_Appli.Model
{
    public class Access
    {
        public int Id { get; set; }

        public int EmployeID{

            get { return EmployeID; }
            
            set { EmployeID = value; OnPropertyChanged("EmployeID"); }
        }

        public int RoomID
        {
            get { return RoomID; }
            set { RoomID = value; OnPropertyChanged("RoomID"); }
        }

        public string OpenTime
        {
            get { return OpenTime; }
            set { OpenTime = value; OnPropertyChanged(OpenTime); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }

        }
    }
}
