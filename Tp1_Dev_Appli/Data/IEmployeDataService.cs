﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Tp1_Dev_Appli.Model;
using System.Collections.ObjectModel;

namespace Tp_Dev_Appli.Data
{
    public interface IEmployeDataService
    {
        IEnumerable<Employe> GetAll();
    }
}