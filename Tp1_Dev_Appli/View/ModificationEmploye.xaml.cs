﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tp1_Dev_Appli
{
    /// <summary>
    /// Logique d'interaction pour ModificationEmploye.xaml
    /// </summary>
    public partial class ModificationEmploye : UserControl
    {
        public ModificationEmploye()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_Confirme_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Confirme = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Confirme;
            Go_Confirme.SelectedViewModel = new ConfirmeViewModel();
        }

        private void Btn_Annule_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Back = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Back;
            Go_Back.SelectedViewModel = new EmployeViewModel();
        }
    }
}
