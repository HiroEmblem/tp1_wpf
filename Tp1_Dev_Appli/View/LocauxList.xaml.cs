﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tp1_Dev_Appli
{
    /// <summary>
    /// Logique d'interaction pour Locaux.xaml
    /// </summary>
    public partial class LocauxList : UserControl
    {
        public LocauxList()
        {
            InitializeComponent();
        }

        private void Button_Ajout_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Ajout = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Ajout;
            Go_Ajout.SelectedViewModel = new AjouterLocalViewModel();
        }

        private void Button_Modif_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Modif = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Modif;
            Go_Modif.SelectedViewModel = new ModifierLocalViewModel();

        }

        private void Button_Supp_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Supp = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Supp;
            Go_Supp.SelectedViewModel = new SupprimerLocalViewModel();

        }
    }
}
