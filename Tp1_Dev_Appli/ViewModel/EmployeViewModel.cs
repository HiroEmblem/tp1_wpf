﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Tp1_Dev_Appli.Model;
using System.Collections.ObjectModel;

namespace Tp1_Dev_Appli
{
    public class EmployeViewModel : INotifyPropertyChanged
    {

        private Employe _employe;
        private static EmployeViewModel _instance = new EmployeViewModel();
        public static EmployeViewModel Instance { get { return _instance; } }

        public Employe Employee
        {
            get { return _employe; }
            set { _employe = value; OnPropertyChanged("Employee"); }
        }
      
        

        private ObservableCollection<Employe> _employes;

        public ObservableCollection<Employe> Employes
        {
            get
            {
                return _employes;
            }
            set
            {
                _employes = value;
                OnPropertyChanged("Employes");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)

        {

            if (PropertyChanged != null)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(propName));

            }

        }
    }
}


