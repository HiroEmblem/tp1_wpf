﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Tp1_Dev_Appli
{
    public class NavigationViewModel : INotifyPropertyChanged
    {
        public ICommand EmployeCommand { get; set; }
        public ICommand LocauxCommand { get; set; }
        public ICommand AjouterEmployeCommand { get; set; }

        private object selectedViewModel;
                    
        public object SelectedViewModel
        {
            get { return selectedViewModel; }

            set { selectedViewModel = value; OnPropertyChanged("SelectedViewModel"); }
        }
            
        public NavigationViewModel()
        {
            LocauxCommand = new BaseCommand(OpenLocaux);
            EmployeCommand = new BaseCommand(OpenEmploye);
            AjouterEmployeCommand = new BaseCommand(OpenAjoutEmploye);
        }

        private void OpenLocaux(object obj)
        {
            SelectedViewModel = new LocauxViewModel();
        }

        private void OpenEmploye(object obj)
        {
            SelectedViewModel = new EmployeViewModel();
        }

        private void OpenAjoutEmploye(object obj)
        {
            SelectedViewModel = new AjouterEmployeViewModel();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)

        {

            if (PropertyChanged != null)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(propName));

            }

        }

    }
}
