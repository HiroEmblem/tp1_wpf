﻿#pragma checksum "..\..\EmployeAjout.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "FFD3159B6AA377950BC9B0EDBE7B466772C7D624"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Tp1_Dev_Appli;


namespace Tp1_Dev_Appli {
    
    
    /// <summary>
    /// EmployeAjout
    /// </summary>
    public partial class EmployeAjout : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grd_EmployeAjout;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle Rect_Employe;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle Rect_Locaux;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Titre;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Employe;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Locaux;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle Rect_Centre;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn_Annule;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Nom;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Prenom;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_NumLocal;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Nom;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Prenom;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle Rect_HautSeparateur;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Periode;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_TitreAcces;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Periode;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn_Confirme;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Cmb_NumLocal;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\EmployeAjout.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle Rect_BasSeparateur;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Tp1_Dev_Appli;component/employeajout.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EmployeAjout.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Grd_EmployeAjout = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.Rect_Employe = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 3:
            this.Rect_Locaux = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 4:
            this.Lbl_Titre = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.Lbl_Employe = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.Lbl_Locaux = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.Rect_Centre = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 8:
            this.Btn_Annule = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.Lbl_Nom = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Lbl_Prenom = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.Lbl_NumLocal = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.TxtBox_Nom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.TxtBox_Prenom = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.Rect_HautSeparateur = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 15:
            this.Lbl_Periode = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.Lbl_TitreAcces = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.TxtBox_Periode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.Btn_Confirme = ((System.Windows.Controls.Button)(target));
            return;
            case 19:
            this.Cmb_NumLocal = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 20:
            this.Rect_BasSeparateur = ((System.Windows.Shapes.Rectangle)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

