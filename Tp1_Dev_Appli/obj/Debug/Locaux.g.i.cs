﻿#pragma checksum "..\..\Locaux.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "811611FA921FF2D150C2428164906C97ECFB5648"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Tp1_Dev_Appli;


namespace Tp1_Dev_Appli {
    
    
    /// <summary>
    /// Locaux
    /// </summary>
    public partial class Locaux : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grd_Locaux;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Lbl_Titre;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid Grd_ListeLocaux;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtBox_Recherche;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn_Ajout;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn_Modif;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Locaux.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Btn_Supp;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Tp1_Dev_Appli;component/locaux.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Locaux.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Grd_Locaux = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.Lbl_Titre = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.Grd_ListeLocaux = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.TxtBox_Recherche = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Btn_Ajout = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\Locaux.xaml"
            this.Btn_Ajout.Click += new System.Windows.RoutedEventHandler(this.Button_Ajout_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Btn_Modif = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\Locaux.xaml"
            this.Btn_Modif.Click += new System.Windows.RoutedEventHandler(this.Button_Modif_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.Btn_Supp = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\Locaux.xaml"
            this.Btn_Supp.Click += new System.Windows.RoutedEventHandler(this.Button_Supp_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

